package trip

import (
	"context"
	trippb "coolcar/proto/gen/go"
)

//type TripServiceServer interface {
//	GetTrip(context.Context, *GetTripRequest) (*GetTripResponse, error)
//	mustEmbedUnimplementedTripServiceServer()
//}

type Service struct{}

func (s *Service) mustEmbedUnimplementedTripServiceServer() {
	//TODO implement me
	panic("implement me")
}

func (s *Service) GetTrip(c context.Context, req *trippb.GetTripRequest) (*trippb.GetTripResponse, error) {

	return &trippb.GetTripResponse{
		Id: req.Id,
		Trip: &trippb.Trip{
			Start:       "abc",
			End:         "def",
			DurationSec: 3600,
			FeeCent:     10000,
		},
	}, nil
}
